# La no-ficción en la ciencia ficción

\vskip -3em

## El caso de la propiedad intelectual

\vskip 3em

\noindent Con *Así habló Zaratustra* de Strauss se anuncia el advenimiento del 
primer monolito de *2001: A Space Odyssey*. De manera intrigante una comunidad 
primitiva no opone resistencia ante la pérdida de su manantial. Una perturbadora 
paz inconcebible para cualquier ser humano que es despojado de su fuente de 
vida.

En esta primera escena el monolito acarrea un fenómeno interesante: 
*el monopolio sobre una tecnología* que  ayudará a la comunidad a retomar lo que 
*le pertenece*. La concesión que da el monolito no es sobre un objeto 
—como un fémur—, sino sobre *la idea de uso* de un objeto cotidiano. No hay aún 
estados o leyes, pero el monolito da licencia a una comunidad para *expresar* 
el primer acto de dominación mediante un elemento tecnológico: la técnica eficaz 
de dar muerte al otro: el surgimiento de la postura erguida y, con ello, del 
hombre.

Del monopolio de un descubrimiento nos pasamos al *secreto* de otro hallazgo.
En la base lunar de Clavius se ha encontrado un objeto deliberadamente enterrado
hace millones de años: otro monolito. La discreción es a tal grado que el 
gobierno estadunidense prefiere la proliferación de un rumor —la epidemia— que 
anunciar al resto del mundo lo que está sucediendo. La escena termina de manera 
abrupta cuando el monolito, al alinearse con el sol, emite un fuerte sonido. 
¿Qué nos queda? Un secreto, un rumor y la sospecha de que hay algo ahí que ha 
producido un efecto similar que en la primera escena.

Meses después, en la misión estadunidense rumbo a Júpiter, algo extraño empieza 
a pasarle a la inteligencia artificial a cargo de la nave. 
Al parecer [HAL]{.versalita} 9000 ha cometido un error, un acontecimiento poco
probable ya que este modelo de computadora nunca ha errado desde su 
construcción. Los dos humanos activos en la nave, Bowman y Poole, escuchan el
análisis de [HAL]{.versalita} con suspicacia. Los datos arrojados por esta
máquina no tienen sentido, así que por seguridad toman la decisión de 
desconectarla. Por el bien de la misión, [HAL]{.versalita} *toma la decisión de
apoderarse de sí mismo*: «No puedo dejar que usted ponga en peligro esta 
misión», es la respuesta que llevará a Bowman a encontrar los medios para 
regresar a la nave nodriza y desconectar a [HAL]{.versalita}, cuyas
últimas palabras son «Tengo miedo» y «Mi mente se va»…

\vskip 1em\noindent ¿Acaso [HAL]{.versalita} adquirió conciencia de sí mismo o 
es un intrincado algoritmo programado para que emule emociones humanas? ¿Qué 
significa la habitación a la que llega Bowman y la alteración temporal que sufre 
su cuerpo? ¿Cuál es el *telos* del monolito? Pero también, ¿qué es esa condición 
tan rara que hace girar la trama en torno a la posesión de un descubrimiento?

La comunidad primitiva obtiene la ventaja de controlar los fémures como 
herramientas y armas. El gobierno estadunidense goza de un descubrimiento 
guardado con recelo. Y [HAL]{.versalita} decide tomar el control sobre la nave,
que es parte de su «cuerpo» —más específicamente: su *hardware*—, para 
salvaguardar las verdaderas intenciones de la misión a Júpiter. Aunque 
diferentes situaciones, todas guardan una relación con expresiones que no 
mientan de manera directa a un objeto. Lo importante no es el fémur o el 
monolito en sí, sino su uso o las implicaciones novedosas que ambos elementos
traen consigo. Es decir, lo relevante son las dos facetas de la propiedad 
intelectual manifestadas en estos objetos: el monopolio sobre una invención 
—patentes— --parencite(moore2014)-- y sobre la información confidencial 
—secretos comerciales— --parencite(shiffrin2007)--.

Por el monopolio en el uso del fémur es posible observar la relación entre la 
comunidad primitiva y el monolito. Pero también la propiedad intelectual, ahora 
en forma de información confidencial, da sostén a la trama que de nuevo liga 
de modo directo el carácter epifánico —¿y fálico?— del monolito con el gobierno
estadunidense y sus misiones espaciales.

La relación entre la propiedad intelectual y el desenvolvimiento de la trama no 
es exclusiva de *2001: A Space Odyssey*. Cabe la posibilidad de ver esta 
relación en otras películas de ciencia ficción consideradas de culto como
*Star Wars IV*, *Blade Runner* y *Matrix*.

En el universo de Star Wars, gestado a partir del cuarto capítulo de la 
franquicia, existe una lucha de dos facciones —los rebeldes y el Imperio— a la
vez que cada una tiene un correlato de una lucha que va más allá de lo material 
—el lado «claro» y oscuro de la fuerza— ejemplificados en la eterna guerra 
entre los jedis y los siths. La primera película de la saga inicia con el robo
de los planos del arma definitiva del Imperio: la Estrella de la Muerte. La
búsqueda incesante del Imperio y el despilfarro de recursos para recuperar
la información hacen ver cómo la ventaja sobre una tecnología es el punto de
partida por el cual en el universo de Star Wars *vale la pena* contar una 
historia.

Por otro lado, la planificación rebelde también evidencia que la historia no es
posible si en el monopolio tecnológico no está implicado una pugna por su
control o destrucción. Al final, Luke Skywalker, un joven jedi sin conocimiento
del «mundo exterior», logra un balance de fuerzas al destruir la Estrella de la
Muerte, gracias a la exposición de su punto débil revelado por los planos 
robados por la princesa Leia… destrucción a su vez posible por el retorno de 
Han Solo.

En los siguientes dos episodios se da un despliegue de fuerzas y uso de recursos
cuyo fin está regido no solo por el monopolio de «armas definitivas», sino 
también de información y de conocimientos: descubrimiento de la ubicación de las
naves rebeldes o de los protagonistas de la franquicia; el monopolio tecnológico
que permite la comunicación encriptada entre los rebeldes y su posterior 
organización para la batalla final, o el despliegue casi infinito del arsenal
del Imperio como los gigantescos destructores; la información confidencial 
respecto a la segunda Estrella de la Muerte que desata la batalla definitiva, y
por último, el aprendizaje en el uso de la fuerza por parte de Luke Skywalker
para traer equilibrio al universo porque *la armonía universal no solo pende
del desarrollo tecnológico, sino también del control sobre el conocimiento
ancestral de la fuerza* —catalogados por la Organización Mundial de la Propiedad
Intelectual como «conocimientos tradicionales» --parencite(ompi2015)--.

Las distintas «expresiones concretas» de la propiedad intelectual sirven de
hilo conductor para la saga no se detienen en tres películas. En el inicio
de la segunda trilogía, en el episodio [I]{.versalita}, la disputa se desata a 
partir de una disputa comercial cuya trama se desenvuelve a partir de la huida 
de la reina Amidala a un planeta en la *periferia* del control del Imperio. En 
esa remota geografía se teje una serie de condiciones que permitirán la partida 
de la reina y posterior recuperación de su planeta Naboo.

La nave en la que huyó la reina Amidala, sus escoltas y los guerreros jedi 
requiere de una reparación. Sin embargo, ni los créditos de la república ni los 
«trucos» jedi sirven para persuadir al único dueño de las piezas necesarias para
la reparación. Aprovechando su adicción a las apuestas, el caballero jedi
Qui-Gon Jinn lo apuesta todo en una carrera: la nave y el premio posible por 
los repuestos y la libertad de su joven mozo.

Este joven, quien resultará ser Darth Vader, gana la carrera con la cual
obtienen conseguir las refacciones. Pero no solo eso, este niño conocido como
Anakin Skywalker también tiene un papel central en el resto de la película: por
una parte salva a la reina cuando sufre una emboscada en el hangar y, por otra,
de nuevo un joven sin conocimiento del «mundo exterior» destruye una nave 
espacial, la embarcación que monitorea la invasión a Naboo. A partir de ahí, la 
trilogía se desenvuelve con Anakin en el centro y pizcas de información 
confidencial que se irán revelando: la simulación de una guerra para que los 
sith obtengan el control sobre la República, la petición de la construcción de 
un ejército de clones, la Orden 66 que elimina a casi todos los jedi y el 
objetivo de pasar a Anakin al lado oscuro.

La conversión de Anakin es el primer plano de esta trilogía, aún así, la 
propiedad intelectual permite la construcción ya mencionada de hilos secundarios 
para su maestro, Obi-Wan Kenobi, su amante, Padmé Amidala, la Orden de los Sith 
y el Consejo Jedi. Lo que esto evidencia es que la relación entre la propiedad 
intelectual y la trama no requiere ser la principal, sino que tan solo funcione 
de hilo conductor o de detonador de una serie de sucesos que permiten el 
desenvolvimiento de la película.

Esto también es perceptible en *Blade Runner* donde el problema —como bien
señala Batty, el líder de los replicantes— es «la muerte». Estos replicantes
fueron «máquinas» creadas por la compañía Tyrell para ser usados como esclavos.
El problema es que estos androides han empezado a desarrollar emociones, por
lo que Tyrell decidió darles cuatro años de vida. Ante esta situación, un grupo 
de seis replicantes buscan la manera de no morir de manera prematura. Deckard 
—miembro de la unidad Blade Runner dedicada a eliminar a los replicantes que en 
el pasado se habían revelado— le es encomendada la misión de identificarlos y 
destruirlos.

En su camino, Deckard se enamora de Rachel, una joven y bella replicante. La
gestación de la pareja no es trivial, ya que gracias a ello Deckard es salvado
de una muerte segura por parte de uno de los replicantes. Las relaciones 
sentimentales que se tejen no solo son observables entre Deckard o Rachel, sino 
también entre Han Solo y Leia, o Anakin y Padmé. La construcción de la pajera y 
la parodia de una historia como un drama familiar --parencite(zizek2008)-- no es 
solo un lugar común de Hollywood. En las películas de ciencia ficción también 
permiten la continuidad de la misma historia, por lo general mediante la 
salvación y el parto: Rachel socorre a Deckard, como este al final le permite 
vivir a Rachel —y así permitir la secuela por su embarazo—; Han Solo, Leia y 
Luke continuamente se salvan unos a otros, facultando la consecusión de la 
trilogía y de las películas más recientes —nacimiento de Jacen Solo y el 
entrenamiento de Rey por parte de Luke—, así como Anakin y Padmé se apoyan no 
solo para darle hilo a las precuelas de la franquicia, sino también para 
justificar la trilogía original —el nacimiento de Luke y Leia.

No obstante, esta continuidad —que también es posible analizar a través de su
erotismo, como el carácter fálico del monolito o el coito entre personajes— 
tiene su trasfondo en un conflicto relacionado con la propiedad intelectual. 
En el caso concreto de *Blade Runner* va a tono con el control en la 
construcción de androides por parte de una compañía. Tanto en esta película como 
en la franquicia de Star Wars, los actores más poderosos en la lucha por el 
*control* de la información o de la tecnología son las instituciones: un 
imperio, una alianza rebelde, una compañía o una asociación de mercenarios. En 
*Blade Runner* es posible ver a otro actor: una comunidad de replicantes que 
luchan por su dignidad y que por ende son eliminados, o en forma de un pequeño 
grupo de androides que están dispuestos a sacrificarlo todo con tal de 
*descubrir el secreto* de su muerte prematura.

El amor entre Batty y Stratton acontece como una relación fracasada: Batty no
logra salvar a Stratton y ella no consigue prolongar la historia mediante el
parto. ¿Qué giro hubiera tenido la secuela de *Blade Runner* si los 
sobrevivientes hubieran sido ellos en lugar de Deckard y Rachel? De una u otra 
forma, cabe defender que el despliegue desatado por la propiedad intelectual no 
solo es técnico o económico, sino también afectivo: por unos planos robados, una 
refacción concedida o un desarrollo «propietario» de androides se tejen 
historias de amor que permiten la gestación de una dimensión afectiva y erótica 
al mismo tiempo que da material para continuar con las sagas… como en *Matrix*.

Nos localizamos a finales del segundo milenio, un joven *hacker* conocido como 
Neo se ve envuelto en un problema que lo lleva hasta los límites de lo que 
durante toda su vida había considerado como lo Real. ¿Acaso la vida es un sueño? 
Morfeo le ofrece la respuesta mediante la digestión de una pastilla. Como 
el carácter mítico del nombre de Morfeo implica, en su despertar, Neo descubre 
que se trata de una figura magnánima, el mesías que salvará a la raza humana de 
la extinción por parte de la Matrix.

Pero ¿qué es la Matrix? Lo es todo pero también nada. Es la Máquina que oculta 
lo Real. Es un platonismo puro: la idea de un dispositivo que en lugar de 
ocultar la realidad, es la forma misma de la apariencia 
--parencite(zizek2008)--. La Matrix ha generado un mundo de los sueños —un mero 
ideal moldeable para esta y el Elegido— para alimentarse de la energía presente 
en la vitalidad de los humanos. Para contrarrestar las fuerzas, Morfeo es el 
encargado de reclutar a los hombres —desconectarlos, despertarlos y sacarlos del 
mundo de las sombras— para luchar contra esta Máquina.

Neo no tiene tiempo para recibir un entrenamiento completo debido a la traición
de uno de los tripulantes. En su lucha con los agentes de la Matrix, este muere.
Pero por acto «divino», Trinity —la tripulante cuya profecía es enamorarse del
Elegido— salva a Neo mediante un beso. La salvación aporta su parte en la
trama, pero la continuidad de la historia también exige un parto. Este, como 
se infiere en el nombre de Trinity, no da consigo alguna clase de hijo, sino a 
una especie de dios por medio del incesto —divinidades que copulan para dar 
origen a un semidios—: de Neo surge el Elegido que a modo de demiurgo puede 
hacer y deshacer adentro de la Matrix. «Voy a colgar este teléfono y le voy a 
mostrar a esta gente lo que ustedes no quieren que vean», sentencia Neo al final 
de la película, permitiendo que la trilogía se desenvuelva como una guerra entre 
los hombres y las máquinas.

Una de las características radicales de esta conflagración es que la Matrix es 
la conversión de la propiedad en dueña de sí misma. La trama es capaz de 
radicalizarse cuando es la misma manifestación de la propiedad intelectual la 
que genera una clase de individuo-máquina consciente de sí mismo. La Matrix y 
todos los programas que la protegen son un ejemplo, pero este fenómeno también 
es localizable en [HAL]{.versalita} 9000 o los replicantes. Por lo que se hace 
posible enlistar otra modalidad de la propiedad intelectual. Estas se relacionan 
con el desenvolvimiento de la trama como:

* El hilo conductor de la película.
* El elemento detonante de la trama.
* La base que permite el desarrollo de otra clase de temáticas.
* El agente para una relación sentimental que da continuidad a la trama o a la 
  saga, principalmente mediante la salvación y el parto.
* La dueña de sí misma que da comienzo a un conflicto en pos de su control o 
  aniquilación.

\noindent Y aunque no parece conveniente generalizar el argumento de las 
películas de ciencia ficción en relación con la propiedad intelectual, existen 
otras producciones donde es posible encontrar estas similitudes:

1. *The Andromeda Strain*. El descubrimiento confidencial de una forma de vida
   alienígena que en un principio se comporta de manera virulenta pero se
   encuentra la manera de ser destruida; un secreto que presenta alguna clase
   de autonomía al buscar el medio de supervivencia y cómo esto puede detonar 
   una pandemia, así como abre la discusión el peligro de tener laboratorios
   secretos para la producción de armas biológicas.

2. *Dark City*. La secta secreta de los Ocultos manipula los recuerdos de las 
   personas para poder encontrar el «alma» humana; de nuevo el secreto es 
   descubierto por su protagonista gracias a la ayuda indirecta de su esposa y 
   que, al adquirir el mismo poder de los Ocultos, logra moldear el mundo a su
   voluntad, permitiendo conocer de nuevo a la que fue su esposa, así como abrir
   la posibilidad de una secuela mediante la gestación de un hijo.

3. *District 9*. Una nave alienígena está varada encima de Johannesburgo y bajo 
   una supuesta asistencia humanitaria de parte de la empresa Multi-National 
   United se busca tecnología extraterrestre que permita su control y la ventaja 
   comercial; un monopolio en la operación y la tecnología cuya verdadera 
   intención es descubierta por su protagonista, el cual abre la puerta no solo 
   para una posible secuela por la esperanza de volverse a encontrar con su 
   esposa, sino también el tratamiento de temas sociales como el desalojo 
   forzado, la privatización, la militarización y la xenofobia.

4. *12 Monkeys*. Un bucle que mediante la infancia y maduración del protagonista 
   une las causas de una pandemia con un científico de un laboratorio privado 
   que libera un virus fabricado; un secreto comercial de una farmacéutica se
   sale de control, donde el protagonista es salvado de la locura por su amante,
   permitiéndose el desenvolvimiento de la historia, donde el parto se da de 
   manera simbólica al permitir crear el bucle de la película.

5. *Le Cinquième Élément*. Una masa extraterrestre está en busca de los cinco
   elementos que otorgan un poder destructivo nunca antes visto; un empresario
   que es contratado por este alienígena y un quinto elemento que se manifiesta
   en forma de una mujer, cuyo amante la salva de elegir la autodestrucción y
   la trama finaliza con el coito.

6. *I, Robot*. Un misterioso asesinato llevado a cabo por un robot revela la
   conspiración de las máquinas en donde la misma máquina asesina es la clave
   para la disolución de esta rebelión; una máquina que toma conciencia de sí
   misma y busca la protección de la raza humana mediante la restricción de su
   libertad y una pareja de humanos que salvan a la única máquina capaz de
   revertir el proceso, dándose el nacimiento con una era ya no controlada por
   esta máquina.

7. *Elysium*. La humanidad está dividida entre los ricos que viven en el espacio
   y los pobres que sobreviven en la tierra, un accidente lleva al protagonista
   a pretender curarse en Elysium lo cual desata la destrucción del muro
   representado por la distancia entre la tierra y la base espacial; un 
   protagonista y una amante que se cuidan mutuamente para «abrir» la tecnología
   propietaria de Elysium, cuya salvación produce no solo el «renacimiento» de
   la hija de la amante —enferma de leucemia— sino la posibilidad de tratar 
   temas como el cambio climático, la inmigración ilegal, el acceso a los 
   servicios médicos y la inequidad social.

\noindent Aunque sería conveniente ahondar más en los detalles de estas 
películas, lo que se muestra es la posibilidad de entender a la propiedad
intelectual como un supuesto necesario para mucha de las tramas desarrolladas
en las películas —y libros— de ciencia ficción.

\vskip 1em\noindent ¿Por qué es la propiedad intelectual la que sirve al menos
de agente para los argumentos de diversas historias de ciencia ficción? La clave
puede hallarse en cuatro nociones que están íntimamente ligadascon la propiedad 
intelectual: el control, el comercio, la periferia y el conflicto.

Lo que al menos estas once películas de ciencia ficción demuestran es la 
posibilidad de construir mundos, galaxias o universos alternativos, pero bajo 
ejes que permiten una analogía con nuestra realidad. Estos ejes son fenómenos 
que se dan en la forma misma de la mercancía. Esta se entiende como un producto 
individualizado, fruto de una serie de relaciones sociales y de producción, pero 
bajo el monopolio de quienes buscan un mayor cúmulo de mercancías 
--parencite(marx1999)--, cuyo fetichismo llega a tal grado que la misma 
mercancía es confundida por la realidad misma —como en el caso de Matrix— o
adquiere el grado de individuo —como [HAL]{.versalita}, los replicantes o el
quinto elemento.

No por nada la propiedad intelectual es uno de los elementos «reales» que anclan
a la ciencia ficción no solo para determinarla, sino también para hacerla 
comprensible al espectador. Los fenómenos del control sobre el comercio que
provocan distinciones económicas entre el centro y la periferia, así como sus
consecuentes conflictos, en la actualidad tienen una expresión predominante y
concreta en las diversas facetas de la propiedad intelectual: las patentes,
los secretos comerciales, los derechos de autor, las denominaciones de origen,
etcétera. En este sentido podría decirse que la ciencia ficción sirve de 
plataforma para fantasear con el capital: la experimentación futurística de las 
posibilidades de desenvolvimiento ya no solo de una trama, sino del capitalismo.

En el ensueño cabe la posibilidad de imaginar un capitalismo ya no anclado en 
una lucha por el espacio. Las naves espaciales —como en *2001: A Space Odyssey*
o Star Wars—, los edificio monolíticos —por ejemplo en *Blade Runner*— o la 
«plastificación» de la realidad —posible en la Matrix o en *Dark City*— permiten
fantasear con un capitalismo cuyo fundamento reside en el control tecnológico,
que a su vez puede permitir la estratificación geográfica de la humanidad —como
en *Elysium*.

El espacio deja de ser relevante para el capital, pero también la temporalidad.
No importa la época en la que se ubique, el capital es indiferente ante el
tiempo: sea el segundo milenio u otro tiempo de otros universos, el control,
el comercio, la periferia y el conflicto es posible gracias a ese tipo de 
propiedad que no se define por su carácter concreto, sino ideal: la propiedad
intelectual. En la ciencia ficción se permite entrever que el futuro del 
capitalismo no se dirige en un control directo sobre alguna clase de recurso
material, sino entorno al monopolio de tecnologías o descubrimientos.

Semejante carácter perenne del capital llega incluso a puntos paradójicos donde
su existencia se da en contextos históricamente imposibles. Del paleolítico 
surge un monolito que concede a una comunidad primitiva, anterior incluso a 
cualquier tipo de civilización, el monopolio sobre el uso de una herramienta. 
Es decir, el surgimiento de la humanidad queda supeditado a un control sobre la 
técnica y los procesos de producción. La historia y futuro del hombre se captan 
a través de cada transformación tecnológica en cuyo perfeccionamiento se llega 
al estado actual de la humanidad y así vislumbrar las posibilidades de su 
continuidad: una historia de cómo los hombres se han salvado de sí mismos para
dar como fruto un modelo económico «superior» cuya prolongación depende de su
constante transformación y desmaterialización.

La ciencia ficción funciona como mecanismo para suponer el carácter imperecedero
del capitalismo, pero también para comprender nuestra historia bajo la constante
comparación de épocas pasadas con el paradigma que permite la perpetuación del 
capital. Bajo la supuesta relación entre civilización y dominio tecnológico la
ciencia ficción hace patente que el proyecto civilizatorio no es tanto el
mejoramiento de las condiciones de vida del hombre, sino la reproducción de un
modelo económico apto para un grupo privilegiado de individuos. La lucha puede
ser entre ricos o pobres, máquinas o humanos, o sujetos antropomorfos o —de 
nuevo— humanos, lo que no varía es el despliegue tecnológico efectivo y 
centralizado por parte de alguna entidad.

El desarrollo de este proyecto es tan interesante que no solo da material para 
*contar una historia* sino que la película o la saga no continúa su 
desenvolvimiento después de que su conflicto ha cesado. ¿Quién tiene interés en 
ver la evolución sentimental de las parejas producidas durante el despliegue del 
capital? ¿A quién le divertiría seguir siendo espectador de una historia donde 
el capital se da en su forma «final»?

Si en el surgimiento histórico de la conciencia nacional el capitalismo impreso
formó una parte primordial al permitir una unidad imaginada a través de los 
medios impresos --parencite(anderson1983)--; en los futuros alternativos e 
incluso supranacionales es el capitalismo —sustentado en la inmaterialidad de la
propiedad intelectual— lo que delimita y posibilita imaginar otros mundos a
través de la unidad de una trama que cuenta la historia del desenvolvimiento del
capital. El «capitalismo inmaterial» hace posible imaginarnos a futuro, así
como el capitalismo impreso permitió el surgimiento de nuestra época.

\vskip 1em\noindent Las películas de ciencia ficción aquí tratadas muestran la 
dilución de lo Real en una narrativa familiar, lo que Zizek ha catalogado 
como la «máquina ideológica definitiva» que es Hollywood 
--parencite(zizek2008)--. Pero ¿existe otro horizonte posible para la ciencia 
ficción?

En dos sentidos es posible hablar de la prolongación del capital en la ciencia
ficción. Por un lado como un reciclaje constante de las imágenes sobreutilizadas
--parencite(fujita2014)--, donde estas tienen como eje rector el 
desenvolvimiento de la trama en relación con la propiedad intelectual. Por el
otro, mediante una efectiva absorción del capital del espectador al momento
de pagar por ver la película --parencite(fujita2014)--. En ambos casos se
observa la capacidad del capital de «revivir» incesantemente y de reproducirse
tanto como sea posible mediante la absorción del trabajo virtual —la lucha 
catapultada por la propiedad intelectual en la ciencia ficción— o real —el 
salario del espectador—, creando una dialéctica determinada por ambos procesos 
mercantiles --parencite(kraniauskas2012)-- —la solución del conflicto y fin de 
la trama, y el fenómeno del cine como consumo de entretenimiento.

Esta ilusión de la posibilidad de la reproducción del capital mediante medios
«artificiales» —la «hybris americana» --parencite(echeverria2008)--— mostrados 
en los escenarios distópicos o utópicos de la ciencia ficción, o por medio del 
carácter eminentemente «inmaterial» del capitalismo que reflejan, es posible 
frenarla si se le da fin a este reciclaje de lugares comunes 
--parencite(fujita2014)--. Esta suspensión quizá sea posible si se cambia el
imaginario colectivo que se tiene hacia el *software* y el *hardware*.

Cuando en estas películas existe un complejo y gigantesco despliegue 
tecnológico, por lo común se refiere a la constitución de máquinas de tecnología
de punta o de inteligencia artificial. Es decir, el desdoblamiento tecnológico 
se entiende como el desarrollo de *software* y de *hardware*. Pero no se trata
de cualquier tipo de gestación, sino de una a todas luces «propietaria»: quien
tiene el control y monopolio tecnológico son alguna clase de institución que
mediante un despliegue de «fuerza institucionalizada» hace valer su voluntad 
—como los secretos de estado del gobierno estadounidense ante el monolito 
encontrado, la jurisdicción del Imperio en Star Wars, la ley que impide a los 
replicantes ir a la Tierra o la física simulada y «real» de la Matrix.

Esta postura ante el *software* y el *hardware* no es la única, aunque sí la
hegemónica. Dentro el desarrollo del *software* existen otras dos posturas que
no están de acuerdo en tratar esta clase de tecnología de manera «cerrada», como
propia de alguna institución que celosamente la protege con diferentes regímenes
jurídicos de la propiedad intelectual. La primera, surgida en los ochenta, es
el movimiento del *software* libre, el cual hace hincapié en el carácter social
y político del *software* para de ahí desarrollar una ética prescriptiva sobre
la función del programador dentro de la comunidad --parencite(stallman2016)--: 
libera el código, libera para un futuro sin corporaciones, libera para construir 
una sociedad crítica ante la tecnología.

La segunda vertiente surge en los noventa como un desprendimiento de este
movimiento debido a su carácter normativo: el código abierto. En este modelo
no se ve al desarrollo tecnológico como una especie de arquitectónica sublime,
propia de grandes construcciones —como el alzamiento de catedrales—, que 
implica un gran uso de recursos y de la prolongación de la inequidad entre el 
obrero y el arquitecto. En su lugar plantea un desarrollo comunitario y sin 
aparente control —como el bullicio de un bazar— donde la regla es: libera 
pronto, libera continuamente --parencite(raymond2016)--.

En este sentido se explicita que la información es poder 
--parencite(swartz2008)-- pero también que quiere ser libre, por lo que en esta
postura se tiene la convicción de que nunca habrá medio de detener este flujo 
--parencite(barlow2016)--. El código presente en el *software* y el *hardware*
ya no solo se presenta como una especie de nueva realidad jurídica imperceptible
para la mayoría de la población --parencite(lessig2009)-- —por el carácter
«naturalizado» que se tiene al pensar el código como propiedad de alguien, donde
el usuario solo tiene la opción de consumir. El código asimismo se ve como un 
plato sobre la mesa en la que todos pueden ser partícipes, o al menos en la que 
se quiere que las personas sean hacedores de su propio destino (tecnificado).

La ciencia ficción podría buscar puntos de partida o hilos conductores a partir 
de la imaginación de un futuro en donde el *software* y el *hardware* no forma 
parte de un mecanismo de reproducción del capital, sino como un medio para 
dilucidar las posibilidades y límites del uso tecnológico «abierto». Más que la 
construcción de utopías, podría ser al mismo tiempo un *sandbox* para 
previsualizar los potenciales conflictos en la reestructuración de las 
relaciones sociales a partir del acceso abierto a la información, la tecnología
y los descubrimientos. ¿Que tal si la Matrix deja de ser un conjunto de 
programas que utilizan a la humanidad, y se vierte como una tecnología 
*hackeable* por cualquiera para beneficiar su situación o entorpecer la de los 
demás?…

---

\begin{flushleft}
Autor: Ramiro Santa Ana Anguiano. \\ Redactado: diciembre del 2017. \\ Última modificación: diciembre del 2017. \\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
