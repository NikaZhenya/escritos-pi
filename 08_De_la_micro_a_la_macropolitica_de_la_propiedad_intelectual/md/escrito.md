# De la micro a la macropolítica de la propiedad intelectual

\vskip 3em

\noindent La propiedad intelectual ([PI]{.versalita}) se define
como «expresión concreta de una idea» --parencite(ompi2016a)--.
Con este término se pretenden englobar una serie de elementos
como los derechos de autor, las patentes, los secretos comerciales,
las marcas, las denominaciones de orígenes, etcétera. Aunque
por lo general la [PI]{.versalita} se percibe como un tema jurídico
y legislativo, existen elementos suficientes para también considerarlo
desde ámbitos sociales, políticos, culturales y filosóficos.

La investigación que he estado realizando para la Maestría en
Filosofía se ha inclinado en criticar a la supuesta «teoría»
de la [PI]{.versalita} que se dice estar fundada en el quehacer
filosófico. Hablan de fundamentos filosóficos cuando por lo general
quieren decir _fragmentos_ en las obras de Locke y Hegel, quizá
un poco de Kant o de Humboldt o de varios utilitaristas.

Mi decisión de tomar el curso de «[Análisis de las sociedades
contemporáneas desde el pensamiento de Slavoj Žižek](https://gitlab.com/NikaZhenya/maestria-asignaturas/tree/master/semestre_3/63057_t102_curso_analisis-de-las-sociedades-contemporaneas-desde-el-pensamiento-de-slavoj-zizek)»
fue porque este autor tiene fuerte influencias hegelianas y psicoanalíticas.
La tendencia «continental» de la teoría de la [PI]{.versalita}
bebe de la teoría de la propiedad de Hegel, así como existe una
interesante autora anglosajona, Jeanne Schroeder, que en su crítica
hace un nexo entre esta y Lacan --parencite(schroeder1998a,schroeder2004a)--.
Además, Žižek en una de sus obras --parencite(zizek2016a)--
aborda de manera tangencial la lucha entre la propiedad
intelectual y el bien común. Mi investigación me ha orientado
a argumentar que esta lucha se da bajo una serie de supuestos
compartidos que, a ultimadas cuentas, ayudan a reproducir el
sistema económico y político en el que vivimos.

Pero hoy quiero salirme un poco de esa discusión y arriesgarme
a gestar un nexo con el análisis sobre lo que implica la violencia.
Para esto empiezo de manera muy silogística:

1. Si _asentimos_ en que los derechos de autor, las patentes
   y los secretos comerciales son también cuestión política.
2. Si la política también se expresa como violencia.
3. Entonces la [PI]{.versalita} es una forma de violencia.

\noindent No sé qué tan sustentable sea generalizar y argumentar
que de manera intrínseca _la [PI]{.versalita} es violencia_.
Pero creo que sí existen medios para defender que en algunas
situaciones la [PI]{.versalita} se da como violencia o al menos
como su cómplice.

Un aspecto interesante de los teóricos de la [PI]{.versalita}
es que la piensan principalmente como un acto de producción por
parte de _un individuo_. Por el trabajo implicado o por la personalidad
expresada en el producto, se justifican los derechos de propiedad
con Locke o Hegel --parencite(locke2006a,hegel2005a)--. La crítica
que en mi opinión es de las más radicales y creativas en este
campo, la de Schroeder, no deja de reproducir el imaginario donde
el acto de producir es, en un primer momento, una cuestión individual.

Lo curioso es que la [PI]{.versalita} en mayor disputa, debido
a los intereses económicos que la envuelven y la capacidad jurídica
de sus propietarios, es la generada en instituciones. Esta especie
de [PI]{.versalita} no es producida a título individual, pese
a que la pretendida justificación de la [PI]{.versalita} piense
primero en la romántica idea _del_ autor y _su_ obra o _del_
inventor y _su_ descubrimiento.

Otro aspecto interesante de la [PI]{.versalita} viene desde la
ambigüedad de su definición como «expresión concreta de una idea».
Por ejemplo, esta definición no indica a qué clase de «ideas»
pretende abarcar. Para delimitarlo, por convención se perciben
como ideas que están en un punto medio. La [PI]{.versalita} no
pretende proteger expresiones comunes --parencite(hughes1988a)--,
como la idea de lavar ropa. Tampoco puede privatizar expresiones
extraordinarias que muestran aspectos del mundo o que son de
uso común --parencite(hughes1988a)--, como la teoría de la relatividad
o la rueda.

¿Qué clase de «expresión concreta» es sujeta a la [PI]{.versalita}?
Aquellas lo suficientemente innovadoras como para que no se consideren
ordinarias. Pero también esas que no son tan novedosas como para
_transformar_ nuestro mundo. Con esto viene a la mente todos
esos inventos que vemos en los infomerciales, como utensilios
para pelar aguacates y demás cosas que observamos por el insomnio…

Sin embargo, no hay que perder de vista lo que esto implica cuando
sus obras, sus investigaciones o lo creado en Silicon Valley
es sujeto a [PI]{.versalita}. Si son privatizables, si es posible
hablar de derechos de autor o de patentes —de que el contenido
de esta ponencia me pertenece— es porque, al menos en principio
y en los ámbitos jurídico, político y económico, nada de esto
_transforma_ sino que _amplía y reproduce_ los sistemas e instituciones
que sostienen nuestro mundo.

En este sentido, la [PI]{.versalita} como herramienta política
está diseñada para perpetuar, para conservar el modo en como
es nuestro mundo desde por lo menos dos ámbitos: la macropolítica
y la micropolítica. En el primero, por supuesto la [PI]{.versalita}
incentiva la acumulación de capital y su beneficio es proporcional
a cuánta [PI]{.versalita} se tiene. No es fortuito que las
empresas _top_ o los Estados miembros del G-20 sean los que 
más estén preocupados en proteger una de las principales fuentes
de riqueza: la [PI]{.versalita}. Aunque tendría que ahondar
en el tema, por cuestión de tiempos y por la opinión que
sostengo, esta reproducción ideológica y eficaz no se da en
un primer momento en un ecosistema de instituciones, gobiernos
y corporaciones.

La [PI]{.versalita} es generada en esos ecosistemas por _individuos_
de carne y hueso que a cambio de un salario ceden sus producciones
a sus empleadores. Esto ya lo dijo Marx y los marxistas --parencite(marx1867a)--.
Incluso puede rastrearse en Hegel y la dialéctica del amo y del
esclavo --parencite(hegel2003a)--. Por eso quiero enfocarme más
en el ámbito micropolítico: cada uno de nosotros, en la búsqueda
de _cambiar_, de _criticar_ o de _entender_ el mundo terminamos
por amplificar y darle continuidad pese a que, para muchos de
nosotros, no es el mejor posible.

Aquí estoy empleando el término de «micropolítica» como el tipo
de acción política que reside principalmente en la voluntad individual
o que se ejerce afuera de instituciones pero sin llegar a ser
masiva: políticas personales, de barrio, de ciertas comunidades
con fuerte sentido de identidad. «Macropolítica» sería lo allende
a esa esfera, la política ejercida por instituciones públicas
o privadas, que si bien los autores intelectuales pueden ser
detectados, son ejecutadas por personas que rara vez tienen voz
o rostro: burócratas, empleados, ciudadanos, policías, soldados…
Se trata de conceptos ambiguos, por lo que me arriesgo en dejarlos
en ese estado.

\vskip 1em\noindent No es fortuito que los teóricos de la [PI]{.versalita}
y sus críticos centren sus esfuerzos en la micropolítica _del
creador y su creación_. ¿Cuántos de ustedes escriben? ¿Cuántos
de ustedes consideran que los escritos les pertenecen? ¿Cómo
reaccionarían si les dijera que su trabajo es propiedad pública,
de la [UNAM]{.versalita} o de una empresa privada?

A muchos de nosotros el Estado mexicano nos da un salario para
estudiar. Si queremos ser recíprocos con los ciudadanos que a
través del Conacyt nos otorgan una beca, nuestro trabajo debería
pertenecerles. Cuando se titulan, su investigación pasa a pertenecer
a la plataforma de tesis de la [UNAM]{.versalita}. ¿Han visto
las páginas legales de los trabajos que alberga? La Dirección
General de Bibliotecas tiene los derechos reservados de los [PDF]{.versalita}
que les damos.  Si tienen la ¿fortuna? de trabajar en esta institución,
tarde o temprano tendrán que publicar como investigadores. Se
puede pensar que el trabajo realizado es propiedad de la [UNAM]{.versalita}
mediante el eufemismo de «investigador _afiliado a x_».

Hace tiempo la [UNAM]{.versalita} firmó acuerdos con [JSTOR]{.versalita}
y Elsevier que son repositorios académicos privados. El primero
explícitamente menciona que tiene los derechos en el apartado
9.1 de sus _Condiciones de uso_ --parencite(jstor2018a)--, para
el caso de Elsevier, es ambiguo quién se adjudica los derechos
--parencite(priego2017a)--.  Así que si tu trabajo como investigador
de la [UNAM]{.versalita} está en [JSTOR]{.versalita} o Elsevier,
deja de ser claro que sea tuyo o de la [UNAM]{.versalita},  sino
de una empresa estadunidense u holandesa. Uno podría pensar que
[JSTOR]{.versalita} o Elsevier le pagan regalías a la [UNAM]{.versalita},
al final sus investigadores son _los que producen_, ¿no? La realidad
es que la [UNAM]{.versalita} le cede los derechos a [JSTOR]{.versalita}
y _le paga_  a ambas por albergar _su_ trabajo, todo por visibilidad
y por sumas que nuestra institución se niega a hacer públicas
--parencite(priego2017a)--.

A sabiendas o en plena ignorancia, reproducimos este modo de
producir y difundir el conocimiento --parencite(zizek2008a)--.
Al final es más cómodo un jardín intelectual propio el cual cuidar.
Leo, escribo, investigo, debato, argumento y publico _pensando_
que soy el dueño de mis ideas expresadas con bits o tinta. La
metáfora erótica del creador dando luz a su creación, sea _ex
nihilo_ o a partir de los bienes comunes, no deja de generar
esa hermosa idea de que _existe un vínculo intrínseco_, secreto
y privado entre uno y su creación.

La relación entre el creador y su creación es de tal intimidad
que sin justificación suponemos que la relación de una
obra con su autor es cualitativamente distinta a la que tiene
el lector y que, por ello, es más importante. Aunque jurídicamente
nuestra obra no nos pertenece: es bien público, de la [UNAM]{.versalita}
o de una empresa estadunidense u holandesa. A pesar de que económicamente
no nos beneficia directamente: la satisfacción por ser publicado
es mayor a esta carencia de empoderamiento. Sin importar que
en un sentido político no importa qué tan disruptivo sea nuestro
discurso: su potencia es neutralizada y digerida por aparatos
estatales, universitarios o privados para su beneficio.

Aquí hay una muestra de cómo a partir de los derechos de autor
o patentes se ejerce violencia institucional: ¿quieres vivir
de la investigación?, haz de ceder los derechos de tu trabajo.
A cambio, se da el paliativo del reconocimiento, del renombre
y de la visibilidad, mientras que alguna institución usa tu investigación
como parte de su capital político.

Interesante sería ahondar en este aspecto, más si se vincula
con lo que Žižek llama violencia sistemática, en la que por
medio de coerción se imponen relaciones de dominación y de
explotación, o violencia objetiva, por la cual a partir de 
las capacidades productivas de cada persona el capitalismo
se reproduce con un velo de abstracción ideológica --parencite(zizek2009a)--.
Por tiempo prefiero dejar suspendido cómo, por medio del temor
a perder el empleo, a dejar de recibir recursos e incluso a
ser demandado, muchos productores intelectuales se ven de una
u otra manera obligados a mantener este velo.

Me gustaría pasar a otro punto mucho más escueto pero que
puede ser sugerente para entender a los fundamentos de la
[PI]{.versalita}. Este involucra una de las formas más nítidas
y grotescas de violencia: la guerra.

\vskip 1em\noindent La intimidad en el supuesto vínculo intrínseco
entre el creador y su creación no es políticamente neutra: concede
una ventaja. Un ejemplo micropolítico podría ser que el autor,
al todavía no ceder los derechos de explotación de su obra, cuenta
con una preeminencia respecto a sus posibles lectores. Primero, tiene
conocimiento que nadie más cuenta. Segundo, yace la posibilidad
de sacar un beneficio económico antes que otros. Tercero, sueña
que esta ventaja quizá se traduzca en que su nombre tenga un
artículo en la Wikipedia, sino es que en alguna historia de la
Filosofía o en _la_ historia universal. No es una hipérbole:
el anhelo de reconocimiento siempre es un fuerte incentivo para
tornar la producción de conocimiento en herramienta política
para el ego. El capital político es distinto si se es académico
de la Universidad de Nueva York o de Londres, que la de alguna
universidad en Eslovenia. En influencia política, no es lo mismo
escribir en inglés que en esloveno o en español…

Desde un enfoque macropolítico la ventaja que permite la intimidad
en la producción de [PI]{.versalita} provoca desde el cabildeo
por parte de empresas o industrias, hasta la capacidad de ser
los primeros en explotar la acumulación de capital. La [PI]{.versalita}
contenida en la industria estadunidense del cine es tal que sin
problemas Disney puede presionar para extender los derechos de
autor por unas décadas más --parencite(wikipedia2018a)--. La
guerra entre Apple y Samsung en el terreno de patentes provoca
un fuerte traslado de capital entre una y otra, entre [EE. UU.]{.versalita}
y China, tal como sucede en el déficit de [PI]{.versalita} entre
América Latina y los países industrializados --parencite(cerlalc2015a)--.

Y hablando de la guerra, ¿acaso esta no es una industria? Incluso
me atrevería a llamarla «institución primigenia». Parece que
siempre ha estado ahí, como parte de lo que llamamos civilización
o ser humano, pese a que es una actividad destructiva
de lo humano y lo planetario. Usa a la población para sus
propios fines. Mata, masacra y destruye todo a su paso sin importar
el bienestar de las millones de personas que conforman su maquinaria.
Atenta a la vida, a nuestra especie y al mundo y aún así se lleva
a cabo. Destruye antiguas instituciones y otorga la tierra fertilizada
con sangre para erigir nuevas. Es ciega a la política, a la economía
o a cultura: existe como arma de doble filo para la izquierda,
la derecha, el comunismo, el anarquismo, el capitalismo, la cultura
occidental, la de América Latina, etcétera. Es transversal, siempre
la primera en erigirse en un nuevo orden, en destruir el viejo
y en sobrevivir a través de la historia humana.

Si la [PI]{.versalita} tiene su ventaja política a partir de
la intimidad entre el creador y su creación. Si la [PI]{.versalita}
con mayor valor es la producida por instituciones. Si la guerra
no es un acontecimiento histórico y no es solo una industria,
sino una institución global que genera conocimiento y crea dispositivos
para cumplir sus objetivos, en forma de máquinas, de código o
de soldados. Entonces cabe la posibilidad de pensar que la guerra
se beneficia de la intimidad de la producción de sus conocimientos
y en su propia reproducción traducidas en secretismo militar.

Siguiendo las intuiciones de teoría económica de Bataille --parencite(bataille1987a)--,
las hecatombes marcan el ritmo de la economía, la política y
la historia. La guerra, única institución diseñada para producir
catástrofes, se vale de esa intimidad-secretismo de la [PI]{.versalita}
para continuar con la juerga. La [PI]{.versalita} militar pronto
se muestra como un elemento mínimo necesario para que la guerra
se reproduzca. ¿Cómo sería posible tener una ventaja militar
si todos saben qué tienes, dónde estás o qué quieres? ¿Cómo sería
posible tener la hegemonía militar sin el temor fruto de la imposibilidad
de saber qué nos deparan las nuevas tecnologías de la destrucción?

Con probabilidad he ido muy lejos, pero ¿qué tal si la [PI]{.versalita}
no tiene su origen en la propiedad física y privada de la que
hablaba Locke o Hegel, sino en los secretos militares que fueron
el germen para producir grandes civilizaciones que los filósofos
modernos tanto admiraron? En el discurso histórico se ha señalado
que los derechos de autor y las patentes fueron las primeras
manifestaciones de la [PI]{.versalita}. En la búsqueda de una
teoría de la [PI]{.versalita} muchos autores han querido fundamentarla
basada en estos arquetipos que terminan por justificar esta
modalidad de la propiedad privada. Sin embargo, en la historia
ha sido posible rastrear cómo los secretos militares funcionaron
como ventajas política y económica en tiempos de guerra mucho
antes que los derechos de autor o las patentes.

Si de la [PI]{.versalita} se elimina ese romántico relato micropolítico
del creador y su creación y en su lugar se percibe como macropolítica
en pos de una ventaja económica y política. Entonces, existe
la posibilidad de que los orígenes de la [PI]{.versalita} y de
toda propiedad sean rastreables en la institución primigenia
que es la guerra, cuya clave son sus secretos militares. Quizá
para entender más a la [PI]{.versalita} sea necesario enfocarnos
en la intimidad del secretismo comercial, en tiempos de paz,
y del secretismo militar, en tiempos de guerra. La [PI]{.versalita}
así planteada tendría un fuerte nexo con el tema que tanto
nos ocupó en este curso: la violencia.

---

\begin{flushleft} Autor: Ramiro Santa Ana Anguiano. \\ Redactado:
diciembre del 2018. \\ Última modificación: diciembre del 2018.
\\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia
Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
